# galaxy-tools
Tool wrappers for the Galaxy Project (computational biomedical research) - https://galaxyproject.org

Original plink wrapper from https://bitbucket.org/crs4/orione-tools/src/1003fff61bef/custom/plink/ (MIT licensed)

All changes here GPL3 licensed.
