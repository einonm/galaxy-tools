#!/usr/bin/env Rscript

args <- commandArgs(TRUE)

HET = read.table(args[1], h=T, as.is=T)
H = (HET$N.NM. - HET$O.HOM.) / HET$N.NM.
png(args[2])
oldpar = par(mfrow=c(1,2))
hist(H,50)
hist(HET$F,50)
par(oldpar)
dev.off()
