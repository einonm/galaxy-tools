"""
plink2 Galaxy Wrapper

Mark Einon <mark.einon@gmail.com>

based on plink.py version 0.1 (andrea.pinna@crs4.it)
"""

import optparse
import os
import shutil
import subprocess
import sys


def filter_outputs(prefix, extension, newname=''):
    cwd = os.getcwd()
    outdir = 'outputs%s' % extension

    os.mkdir(outdir)
    files = sorted([f for f in os.listdir(cwd) if (os.path.isfile(f) and
                                                   f.startswith(prefix) and
                                                   f.endswith(extension) and not
                                                   f.endswith('log'))])
    for f in files:
            filetype = f.split('.')[-1]
            if extension:
                shutil.move(f, os.path.join(outdir, prefix + '.' + filetype))
            else:
                shutil.move(f, os.path.join(outdir, newname + '.' + filetype))

    print "Moved %d files to %s - %s" % (len(files), outdir, files)


def __main__():
    cmd = 'plink2'

    # Local args are options not passed onto plink, but processed in this file
    local_args = ['input_type',                 'lped_path',                'lped_basefile',   'pbed_path',
                  'pbed_basefile',              'bed_file',                 'bim_file',        'fam_file',
                  'indep_window_size',          'indep_window_step',        'indep_threshold', 'indep_pairwise_window_size',
                  'indep_pairwise_window_step', 'indep_pairwise_threshold', 'bmerge_path',     'bmerge_basefile',
                  'log', 'newname']

    # opt args are options passed directly to plink
    opt_args = ['extract', 'read_genome', 'within',   'exclude',
                'flip',    'geno',        'hwe',      'maf',
                'mind',    'ppc',         'mds_plot', 'out',
                'bed',     'bim',         'fam',      'update_sex',
                'remove',  'ped',         'map']

    # opt flags are flags passed directly to plink
    opt_flags = ['allow_no_sex', 'assoc',    'check_sex', 'cluster',
                 'freq',         'genome',   'hardy',     'het',
                 'logistic',     'make_bed', 'missing',   'mh',
                 'sex']

    print 'Parsing Plink input options...'
    parser = optparse.OptionParser()

    for local_arg in local_args:
        parser.add_option("--%s" % local_arg)

    for arg in opt_args:
        parser.add_option("--%s" % arg)

    for flag in opt_flags:
        parser.add_option("--%s" % flag, action="store_true")

    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')

    # build Plink command to be executed
    for key in vars(options):
        if key in local_args:
            if key == 'input_type':
                if options.input_type == 'lped':
                    cmd += ' --file %s' % os.path.join(options.lped_path, options.lped_basefile)
                elif options.input_type == 'pbed':
                    cmd += ' --bfile %s' % os.path.join(options.pbed_path, options.pbed_basefile)
            if key == 'bmerge_basefile' and vars(options)[key] != None:
                print "path %s, bf %s" % (options.bmerge_path, options.bmerge_basefile)
                bmerge_file = os.path.join(options.bmerge_path, options.bmerge_basefile)
                cmd += ' --bmerge %s' % bmerge_file
            if key == 'indep_window_size' and vars(options)[key] != None:
                cmd += ' --indep %s %s %s' % (options.indep_window_size, options.indep_window_step, options.indep_threshold)
            if key == 'indep_pairwise_window_size' and vars(options)[key] != None:
                cmd += ' --indep-pairwise %s %s %s' % (options.indep_pairwise_window_size,
                                                       options.indep_pairwise_window_step, options.indep_pairwise_threshold)

        elif key in opt_args:
            if vars(options)[key] != None:
                cmd += " --%s %s" % (key.replace('_', '-'), vars(options)[key])

        elif key in opt_flags:
            if vars(options)[key] != None:
                cmd += " --%s" % (key.replace('_', '-'))

    # Execution of Plink
    print 'Executing Plink...'
    log = open(options.log, 'w') if options.log else sys.stdout
    try:
        # need to redirect stderr because Plink writes some logging info there
        subprocess.check_call(cmd, stdout=log, stderr=subprocess.STDOUT, shell=True)
    except subprocess.CalledProcessError:
        # catch this error, as we still may want to keep the output file (e.g. missnp)
        print "plink2 failed"

    finally:
        if log != sys.stdout:
            log.close()

        filter_outputs(options.out, 'bed')
        filter_outputs(options.out, 'bim')
        filter_outputs(options.out, 'fam')
        filter_outputs(options.out, 'missnp')
        filter_outputs(options.out, 'sexcheck')
        filter_outputs(options.out, 'lmiss')
        filter_outputs(options.out, 'imiss')
        filter_outputs(options.out, 'in')
        filter_outputs(options.out, 'out')
        filter_outputs(options.out, 'genome')
        filter_outputs(options.out, 'frq')
        filter_outputs(options.out, 'het')
        filter_outputs(options.out, 'assoc')
        filter_outputs(options.out, '', options.newname)

if __name__ == "__main__":
    __main__()
