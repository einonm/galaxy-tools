import optparse
import os
import shutil

import galaxy.model  # noqa


def generate_primary_file(output_file):
    rval = ['<html><head><title>Galaxy Composite Dataset </title></head><p/>']
    rval.append('<div>This composite dataset is composed of the following files:<p/><ul>')
    rval.append('<li><a href="RgeneticsData.bed" type="application/binary">RgeneticsData.bed</a></li>')
    rval.append('<li><a href="RgeneticsData.bim" type="application/binary">RgeneticsData.bim</a></li>')
    rval.append('<li><a href="RgeneticsData.fam" type="application/binary">RgeneticsData.fam</a></li>')
    rval.append('</ul></div></html>')

    tmp_html = open(output_file, 'w')
    tmp_html.write("\n".join(rval))
    tmp_html.close()


def __main__():
    args = ['bed', 'bim', 'fam', 'files_path', 'output_file']

    parser = optparse.OptionParser()

    for arg in args:
        parser.add_option("--%s" % arg)

    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')

    os.mkdir(options.files_path)

    infiles = {options.bed: 'bed', options.bim: 'bim', options.fam: 'fam'}
    for infile, ext in infiles.iteritems():
        filename = 'RgeneticsData.' + ext
        shutil.copy(infile, os.path.join(options.files_path, filename))

    generate_primary_file(options.output_file)

if __name__ == '__main__':
    __main__()
