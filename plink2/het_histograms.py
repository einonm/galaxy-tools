"""
heterozygosity graphing Galaxy Wrapper

Mark Einon <mark.einon@gmail.com>
"""

import optparse
import os
import subprocess
import sys


def __main__():
    cmd = 'het_histograms.R'

    # Local args are options not passed on, but processed in this file
    local_args = ['output', 'output_id', 'new_file_path', 'run_dir']

    # opt args are options passed directly on
    opt_args = ['het', 'out']

    print 'Parsing input options...'
    parser = optparse.OptionParser()

    for local_arg in local_args:
        parser.add_option("--%s" % local_arg)

    for arg in opt_args:
        parser.add_option("--%s" % arg)

    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')

    # build command to be executed
    cmd = options.run_dir + "/" + cmd

    for key in sorted(vars(options), key=vars(options).get):
        if key in opt_args:
            if vars(options)[key] != None:
                cmd += " %s" % vars(options)[key]

    print 'Executing... %s\n' % cmd
    log = open(options.output, 'w') if options.output else sys.stdout
    try:
        subprocess.check_call(cmd, stdout=log, stderr=subprocess.STDOUT, shell=True)
    except subprocess.CalledProcessError:
        print "Failed\n"

    finally:
        subprocess.call(['mkdir', options.new_file_path], stderr=subprocess.STDOUT)
        cwd = os.getcwd()
        files = sorted([f for f in os.listdir(cwd) if (os.path.isfile(f) and f.startswith(options.out) and not f.endswith('log'))])
        for f in files:
            new_file = "%s/%s" % (options.new_file_path, f)
            subprocess.check_call(['mv', f, new_file])

        print "Successfully moved %d files - %s \n" % (len(files), files)

        if log != sys.stdout:
            log.close()

if __name__ == "__main__":
    __main__()
