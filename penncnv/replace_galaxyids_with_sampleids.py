import optparse
import os
import shutil
import subprocess
import sys
import re

# Annoyingly, galaxy uses it's own filenaming scheme and detect_cnv uses these filenames
#   to identify the sample. This function replaces the galaxy filename ID within the CNV
#   file with the original sample ID taken from the heading of the input split file.
def __main__():
    args = ['infile']

    parser = optparse.OptionParser()

    for arg in args:
        parser.add_option("--%s" % arg)

    (options, other_args) = parser.parse_args()

    if len(other_args) > 0:
        parser.error('Wrong number of arguments')

    out_cnvfile = open('out.file', 'w')

    # lookup table dict for quick access to previously found sample_ids
    conversion_lookup = {}

    ref_re = re.compile(r'(^.*\s)(\/.*dataset_[0-9]*.dat)(.*\n)')
    dataset_id_re = re.compile(r'^Name\sChr\sPosition\s(.*)\.Log\ R\ Ratio.*\n')

    with open(options.infile, 'r') as in_file:
        for line in in_file:
            # find reference to a galaxy dataset filename
            if ref_re.match(line) is not None:
                dataset_ref = ref_re.sub(r'\2', line)
                sample_id = conversion_lookup.get(dataset_ref)
                if sample_id == None:
                    with open(dataset_ref, 'r') as dataset:
                        sample_id = dataset_id_re.sub(r'\1', dataset.readline())
                        conversion_lookup[dataset_ref] = sample_id

                # write line to out_cnvfile
                result = ref_re.sub(r'\1', line) + ref_re.sub(r'%s\3' % sample_id, line)
                out_cnvfile.write(result)

    out_cnvfile.close()

if __name__ == "__main__":
    __main__()

