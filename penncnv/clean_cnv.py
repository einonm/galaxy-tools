import optparse
import subprocess
import sys


def __main__():
    cmd = 'clean_cnv.pl'

    # Local args are options not passed on, but processed in this file
    local_args = ['incnv_file', 'output']

    # opt args are options passed directly to the wrapped program
    opt_args = ['signalfile', 'fraction']

    # opt flags are flags passed directly to the wrapped program
    opt_flags = ['bp']

    parser = optparse.OptionParser()

    for local_arg in local_args:
        parser.add_option("--%s" % local_arg)

    for arg in opt_args:
        parser.add_option("--%s" % arg)

    for flag in opt_flags:
        parser.add_option("--%s" % flag, action="store_true")

    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')

    # build command to be executed
    for key in vars(options):
        if key in opt_args:
            if vars(options)[key] != None:
                cmd += " --%s %s" % (key.replace('_', '-'), vars(options)[key])

        elif key in opt_flags:
            if vars(options)[key] != None:
                cmd += " --%s" % (key.replace('_', '-'))

    cmd += " combineseg %s" % (options.incnv_file)

    log = open(options.output, 'w') if options.output else sys.stdout
    outcnv = open('out.cnv', 'w')
    try:
        # need to redirect stderr because cmd writes some logging info there
        subprocess.check_call(cmd, stdout=outcnv, stderr=log, shell=True)
    except subprocess.CalledProcessError as excep:
        # catch this error, as we still may want to keep the output file
        print "failed - %d\n" % excep.returncode

    finally:
        outcnv.close()
        if log != sys.stdout:
            log.close()

if __name__ == "__main__":
    __main__()
