import optparse
import os
import shutil
import subprocess
import sys


def __main__():
    cmd = 'detect_cnv.pl'

    # Local args are options not passed on, but processed in this file
    local_args = ['new_file_path', 'output']

    # opt args are options passed directly to the wrapped program
    opt_args = ['hmm', 'pfb', 'out', 'gcmodelfile', 'listfile']

    # opt flags are flags passed directly to the wrapped program
    opt_flags = ['test', 'confidence']

    parser = optparse.OptionParser()

    for local_arg in local_args:
        parser.add_option("--%s" % local_arg)

    for arg in opt_args:
        parser.add_option("--%s" % arg)

    for flag in opt_flags:
        parser.add_option("--%s" % flag, action="store_true")

    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')

    cwd = os.getcwd()

    # build command to be executed
    for key in vars(options):
        if key in opt_args:
            if vars(options)[key] != None:
                cmd += " --%s %s" % (key.replace('_', '-'), vars(options)[key])

        elif key in opt_flags:
            if vars(options)[key] != None:
                cmd += " --%s" % (key.replace('_', '-'))

    print 'Executing...\n'
    log = open(options.output, 'w') if options.output else sys.stdout
    try:
        # need to redirect stderr because cmd writes some logging info there
        subprocess.check_call(cmd, stdout=log, stderr=subprocess.STDOUT, shell=True)
    except subprocess.CalledProcessError as excep:
        # catch this error, as we still may want to keep the output file
        print "failed - %d\n" % excep.returncode

    finally:
        os.mkdir('outputscnv')
        files = sorted([f for f in os.listdir(cwd) if (os.path.isfile(f) and f.startswith(options.out))])
        for f in files:
            shutil.move(f, os.path.join('outputscnv', f))

        print "Successfully moved %d files - %s \n" % (len(files), files)

        if log != sys.stdout:
            log.close()

if __name__ == "__main__":
    __main__()
