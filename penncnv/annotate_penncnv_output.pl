#!/bin/env perl
# Author: Elliott Rees <ReesEG@cardiff.ac.uk>
#
# Format rawcnv file, annotate genes and pathogenic loci

use strict;
use warnings;
use Getopt::Long;

my ($file_in, $file_out, $gene_in, $loci_in);
#my $directory="D:/Denovo/zScores/zScores_10_9/zScores";
GetOptions("file-in=s"=>\$file_in,
		   "gene-in=s"=>\$gene_in,
		   "loci-in=s"=>\$loci_in,
		   "file-out=s"=>\$file_out);
		   
open (FILE, "$file_in") or die "file in prob";
open (GENE, "$gene_in") or die "file in prob";
open (LOCI, "$loci_in") or die "file in prob";

open (WRITE, ">$file_out") or die "couldnt write";

my (@file_1,@Gene,@Loci);

while (<FILE>)	{
	my $line = $_;
	chomp $line;
	$line=~s/\r//;
	my @temp = split (" ",$line);
	push (@file_1,[@temp]);
	}
close (FILE);

#read the Gene file
while (<GENE>) { 
	#next if ($.==1);
	my $line = $_;
	chomp $line;
	$line=~s/\r//g;
	my @temp = split(/\t/,$line);
	$temp[0]=~s/chr//g;
	$temp[0]=~s/X/23/g;
	$temp[0]=~s/Y/24/g;
	$temp[0]=~s/M/25/g;
	push (@Gene,[@temp]);
	}
close (GENE);

while (<LOCI>)	{
	next if ($.==1);
	my $line = $_;
	chomp $line;
	$line=~s/\r//;
	my @temp = split ("\t",$line);
	push (@Loci,[@temp]);
	}
close (LOCI);

print WRITE "ID","\t","Location","\t","chr","\t","start","\t","end","\t","Type","\t","Size","\t","Probe","\t","Conf","\t","Pathogenic_Locus","\t","N_genes_hit","\t","Genes","\n";

foreach my $x (@file_1) {
	
	my $location=$x->[0];
	my @splitLocTemp=split (":",$location);
	my $chr=$splitLocTemp[0];
	$chr=~s/chr//g;
	my @splitLocTemp2=split ("-",$splitLocTemp[1]);
	my $start=$splitLocTemp2[0];
	my $end=$splitLocTemp2[1];
	
	my @tempProbe=split("=",$x->[1]);
	my @tempSize=split("=",$x->[2]);
	my @tempType=split("=",$x->[3]);
	my @tempConf=split("=",$x->[7]);
	
	my $Probe=$tempProbe[1];
	my $Size=$tempSize[1];
	$Size=~s/,//g;
	my $Type=$tempType[1];
	my $Conf=$tempConf[1];
	
	
	my %array_genes = ();
	foreach my $gene (@Gene) {
		next if (exists $array_genes {$gene->[3]}); ## check this is correct
		next if($chr ne $gene->[0]);
		if (($start) > $gene->[2] || ($end) < $gene->[1]) {
			next;
		}
		$array_genes {$gene->[3]} = 1;		
		}
	my $N_genes= scalar keys %array_genes;

	my %locusHit = ();
	foreach my $l (@Loci) {
	
		next if (($Type == 1 || $Type == 0) && $l->[1] == 3);
		next if (($Type == 3 || $Type == 4) && $l->[1] == 1);
		next if($chr ne $l->[2]);
		if (($start) > $l->[4] || ($end) < $l->[3]) {
			next;
			}
		$locusHit {$l->[0]} = 1;
		}
	print WRITE $x->[4],"\t",$location,"\t",$chr,"\t",$start,"\t",$end,"\t",$Type,"\t",$Size,"\t",$Probe,"\t",$Conf,"\t",join(",",keys %locusHit),"\t",$N_genes,"\t",join(",",keys %array_genes),"\n";
	}


close (WRITE);





























