import optparse
import os
import subprocess
import sys


def __main__():
    cmd = 'compile_pfb.pl'

    # Local args are options not passed on, but processed in this file
    local_args = ['new_file_path', 'output',  'sigfiles']

    # opt args are options passed directly to the wrapped program
    opt_args = ['out']

    # opt flags are flags passed directly to the wrapped program
    opt_flags = []

    parser = optparse.OptionParser()

    for local_arg in local_args:
        parser.add_option("--%s" % local_arg)

    for arg in opt_args:
        parser.add_option("--%s" % arg)

    for flag in opt_flags:
        parser.add_option("--%s" % flag, action="store_true")

    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')

    # build command to be executed
    for key in vars(options):
        if key in local_args:
            if key == 'sigfiles' and vars(options)[key] != None:
                cmd += ' %s' % (options.sigfiles.replace(',', ' '))

        elif key in opt_args:
            if vars(options)[key] != None:
                cmd += " --%s %s" % (key.replace('_', '-'), vars(options)[key])

        elif key in opt_flags:
            if vars(options)[key] != None:
                cmd += " --%s" % (key.replace('_', '-'))

    print 'Executing...\n'
    log = open(options.output, 'w') if options.output else sys.stdout
    try:
        # need to redirect stderr because cmd writes some logging info there
        subprocess.check_call(cmd, stdout=log, stderr=subprocess.STDOUT, shell=True)
    except subprocess.CalledProcessError as excep:
        # catch this error, as we still may want to keep the output file
        print "failed - %d\n" % excep.returncode

    finally:
        cwd = os.getcwd()
        files = sorted([f for f in os.listdir(cwd) if (os.path.isfile(f) and f.startswith(options.out))])
        log.writelines("%s\n" % f for f in files)
        for f in files:
            file_extension = f.split('.')[-1]
            if file_extension == 'pdf':
                file_type = 'pdf'
            elif file_extension == 'bed':
                file_type = 'bed'
            else:
                file_type = 'txt'

            new_file = "%s/%s_%s" % (options.new_file_path, f, file_type)
            subprocess.check_call(['mv', f, new_file])

        print "Successfully moved %d files - %s \n" % (len(files), files)

        if log != sys.stdout:
            log.close()

if __name__ == "__main__":
    __main__()
